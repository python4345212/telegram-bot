<div id="header" align="center">
  <img src="https://media.giphy.com/media/5rT8xqVLpB6S6Ej89o/giphy.gif?cid=ecf05e471q1ck8apv7d7ugjwf5mrirdwh5f2e6mycudd9567&ep=v1_gifs_search&rid=giphy.gif&ct=g" width="100"/>
</div>

<div id="badges" align="center">
  <a href="https://vk.com/maxderzhavin">
    <img alt="VK Badge" src="https://img.shields.io/badge/%D0%92%D0%BA%D0%BE%D0%BD%D1%82%D0%B0%D0%BA%D1%82%D0%B5-blue?style=flat&logo=vk">
  </a>
  <a href="https://t.me/Maxoodino">
    <img alt="Telegram Badge" src="https://img.shields.io/badge/Telegram-white?style=flat&logo=telegram">
  </a>
  <a href="maxoushke@mail.ru">
    <img alt="Static Badge" src="https://img.shields.io/badge/Mail.ru-orange?style=flat&logo=maildotru">
  </a>
</div>

<!--установка-->
## Установка бота(Linux)
У вас должны быть установлены [зависимости проекта](https://gitlab.skillbox.ru/maksim_derzhavin/python_basic_diploma#зависимости)

1. Клонирование репозитория 

```git clone https://gitlab.skillbox.ru/maksim_derzhavin/python_basic_diploma.git ```

2. Переход в директорию python_basic_diploma

```cd python_basic_diploma```

3. Создание виртуального окружения

```python -m venv bot_venv```

4. Активация виртуального окружения

```source bot_venv/bin/activate```

5. Установка зависимостей

```pip install -r requirements.txt```

6. Создание файла .env

___После создания файла .env в основной директории. Открываем файл и создаем две переменные, в которые записываем ключи___

_В переменной "BOT_TOKEN" в кавычках необходимо записать токен взятый в телеграме у [бота](https://t.me/BotFather)_

```BOT_TOKEN=""```

_В переменную "RAPID_API_KEY" в кавычках необходимо записать API-ключ взятый на API [сайте](https://rapidapi.com/apidojo/api/hotels4)_

```RAPID_API_KEY=""```

7. Запускаем скрипт для демонстрации возможностей бота

```python main.py```

<!--документация-->
## Документация
Пользовательскую документацию можно получить по [этой ссылке](./docs/ru/index.md).

<!--поддержка-->
## Поддержка
Если у вас возникли сложности или вопросы по использованию бота, создайте 
[обсуждение](https://gitlab.skillbox.ru/maksim_derzhavin/python_basic_diploma/-/issues/new) в данном репозитории или напишите на электронную почту - maxoushke@mail.ru

<!--зависимости-->
## Зависимости
Эта программа зависит от интепретатора Python версии 3.7 или выше, PIP 23.2.1 или выше.
