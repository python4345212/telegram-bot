from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from database.adding import add_city_name
from utils.bot_logger import logger_bot
from utils.check_city_and_country import check_populated_area


def place(parameters: dict, chat_id: int) -> InlineKeyboardMarkup | None:
    """
    Функция создания inline-клавиатуры с уточнением городов и их адреса
    :param parameters: Словарь с названием города и языком локализации
    :param chat_id: Идентификатор чата с пользователем
    :return: Клавиатуру при успешном поиске, в противном случае None
    """
    destinations = InlineKeyboardMarkup()
    list_of_cities = check_populated_area(parameters=parameters)
    if list_of_cities is None:
        logger_bot.log_debug(f"Клавиатура с городами создана не была вернули - None")
        return None
    else:
        for id_cities in list_of_cities:
            destinations.add(InlineKeyboardButton(text=id_cities['city_name'],
                                                  callback_data=id_cities['city_id']))
        add_city_name(city_name=list_of_cities, chat_id=chat_id)
        logger_bot.log_info(f"Создана inline-клавиатура с информацией - {list_of_cities}")
        return destinations

