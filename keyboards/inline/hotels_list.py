from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from utils.bot_logger import logger_bot


def hotels(hotel_name: str, hotel_id: int) -> InlineKeyboardMarkup:
    """
    Функция создания inline-кнопок для вывода названий отелей
    :param hotel_name: Название отеля
    :param hotel_id: Идентификатор отеля
    :return: inline-кнопки с текстом названия отеля и обратной информации в виде идентификатора отеля
    """
    destinations = InlineKeyboardMarkup()
    destinations.add(InlineKeyboardButton(text=f'{hotel_name}',
                                          url=f'https://www.hotels.com/h{hotel_id}.Hotel-Information/'))
    logger_bot.log_info(f"Создана inline-клавиатура с информацией - {hotel_name}")
    return destinations
