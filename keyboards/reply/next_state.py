from telebot.types import ReplyKeyboardMarkup, KeyboardButton
from utils.bot_logger import logger_bot


def show_next() -> ReplyKeyboardMarkup:
    """
    Функция reply-клавиатуры для прощания пользователя
    :return: Reply-кнопку
    """
    markup = ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    markup.add(KeyboardButton(text='До свидания'))
    logger_bot.log_info(f"Создана reply-клавиатура с текстом - До свидания")
    return markup
