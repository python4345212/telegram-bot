from telebot.types import ReplyKeyboardMarkup, KeyboardButton
from utils.bot_logger import logger_bot


def sort_param() -> ReplyKeyboardMarkup:
    """
    Функция вывода пользователю reply-клавиатуры
    :return: клавиатуру с текстом "Цена" и "Расстояние"
    """
    params = ("Расстояние", "Цена")
    destination = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    destination.add(KeyboardButton(text=params[0]))
    destination.add(KeyboardButton(text=params[1]))
    logger_bot.log_info(f"Создана reply-клавиатура с текстом - {params}")
    return destination


def sort_method() -> ReplyKeyboardMarkup:
    """
    Функция вывода пользователю reply-клавиатуры
    :return: клавиатуру с текстом "От меньшего к большему" и "От большего к меньшему"
    """
    params = ("От меньшего к большему", "От большего к меньшему")
    destination = ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    destination.add(KeyboardButton(text=params[0]))
    destination.add(KeyboardButton(text=params[1]))
    logger_bot.log_info(f"Создана reply-клавиатура с текстом - {params}")
    return destination
