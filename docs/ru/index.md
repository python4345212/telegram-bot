# Telegram-bot Docs RU
Пользовательская документация Telegram-бота на русском

## Оглавление
1. [Зачем нужен телеграм бот](./info.md)
2. [Команды для пользователя](./user_comands.md)