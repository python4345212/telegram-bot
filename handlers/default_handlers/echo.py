from telebot.types import Message
from loader import bot
from utils.bot_logger import logger_bot


@bot.message_handler(state=None)
def bot_echo(message: Message):
    """
    Эхо хендлер, куда летят текстовые сообщения без указанного состояния
    :param message: Сообщение пользователя
    :return: None
    """
    logger_bot.log_info(f"Пользователь {message.from_user.id} отправил сообщение: {message.text} вне состояния")
    bot.send_message(message.from_user.id, "Эхо без состояния или фильтра.\n" f"Сообщение: {message.text}")
