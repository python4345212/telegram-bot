from telebot.types import Message
from database.adding import get_history
from loader import bot
from utils.bot_logger import logger_bot


@bot.message_handler(commands=["history"])
def user_history(message: Message):
    """
    Функция для команды "history", отправляет пользователю историю его послединх 10-ти запросов
    :param message: Сообщение пользователя
    :return: None
    """
    logger_bot.log_info(f"Пользователь {message.from_user.id} нажал команду /history")
    history = get_history(chat_id=message.chat.id)
    if history is None:
        bot.send_message(message.chat.id, f"История пуста")
    else:
        bot.send_message(message.chat.id, 'История запросов:')
        for number_req, request in enumerate(history):
            bot.send_message(message.chat.id,
                             'Запрос {id_request}:\n'
                             'Название города: {city_name}\n'
                             'Количество взрослых: {adults}\n'
                             'Количество выведенных результатов: {result_size}\n'
                             'Дата въезда в отель: {d_in}\n'
                             'Дата отъезда из отеля: {d_out}'.format(id_request=number_req + 1,
                                                                     city_name=request["info"]["city_id"],
                                                                     adults=request["info"]["adults"],
                                                                     result_size=request["info"]["result_size"],
                                                                     d_in=request["info"]["date_in"],
                                                                     d_out=request["info"]["date_out"]
                                                                     ))
