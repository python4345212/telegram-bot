from telebot.types import Message, ReplyKeyboardRemove
from database.adding import add_user
from loader import bot
from utils.bot_logger import logger_bot


@bot.message_handler(commands=["start"])
def bot_start(message: Message):
    """
    Стартовая функция для команды "start", знакомство с пользователем
    :param message: Сообщение пользователя
    :return: None
    """
    logger_bot.log_info(f"Пользователь {message.from_user.id} запустил бота с командой /start")
    bot.send_message(message.from_user.id, f"Привет, {message.from_user.full_name}! Я ваш помощник для выбора отеля",
                     reply_markup=ReplyKeyboardRemove())
    add_user(user_name=message.from_user.full_name, chat_id=message.chat.id)
    bot.send_message(message.from_user.id, f"Команды для управления ботом: /help")
