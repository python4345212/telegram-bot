from telebot.types import Message
from config_data.config import DEFAULT_COMMANDS
from loader import bot
from utils.bot_logger import logger_bot


@bot.message_handler(commands=["help"])
def bot_help(message: Message):
    """
    Функция для вывода пользователю комманд и информации их назначения
    :param message: Сообщение пользователя
    :return: None
    """
    logger_bot.log_info(f"Пользователь {message.from_user.id} нажал команду /help")
    text = [f"/{command} - {desk}" for command, desk in DEFAULT_COMMANDS]
    bot.send_message(message.chat.id, "\n".join(text))
