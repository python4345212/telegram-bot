import datetime
from loader import bot
from database.adding import get_date_out, get_date_in, get_city_name, get_city_id, get_command
from keyboards.reply.sort_parameters import sort_method
from states.UserState import UserStates
from telebot.types import Message, ReplyKeyboardRemove
from utils.bot_logger import logger_bot


@bot.message_handler(state=UserStates.PhotosCount)
def get_adult(message: Message):
    """
    Состояние записи количества фотографий которое хочет увидеть пользователь. Выбор следющего действия.
    :param message: Сообщение пользователя
    :return: None
    """
    logger_bot.log_info(f"Пользователь {message.from_user.id} отправил сообщение: {message.text}")
    if message.text.isdigit() and 2 <= int(message.text) <= 10:
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['photos'] = message.text
        date_in = datetime.date.fromisoformat(get_date_in(chat_id=message.chat.id))
        date_out = datetime.date.fromisoformat(get_date_out(chat_id=message.chat.id))
        bot.send_message(message.chat.id,
                         f'Выбранное место: {get_city_name(chat_id=message.chat.id, city_id=str(get_city_id(chat_id=message.chat.id)))}\n'
                         f'Дата въезда: {date_in.day}.{date_in.month}.{date_in.year}\n'
                         f'Дата отъезда: {date_out.day}.{date_out.month}.{date_out.year}\n'
                         f'Количество взрослых: {data["adults"]}\n'
                         f'Количество фотографий: {data["photos"]}',
                         reply_markup=ReplyKeyboardRemove())
        if get_command(chat_id=message.chat.id) == '/high':
            bot.send_message(message.chat.id, f'Какую сортировку провести:\n'
                                              f'От меньшего к большему\n'
                                              f'От большего к меньшему',
                             reply_markup=sort_method())
            bot.set_state(message.from_user.id, UserStates.SetFilter, message.chat.id)
        elif get_command(chat_id=message.chat.id) == '/custom':
            bot.send_message(message.chat.id, f'Напишите минимальную стоимость номера, в долларах:')
            bot.set_state(message.from_user.id, UserStates.MinCost, message.chat.id)
    else:
        bot.send_message(message.chat.id, f'Введите число, не менее 2 и не более 10, например: 4')
