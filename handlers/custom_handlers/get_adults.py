import datetime
from telegram_bot_calendar import DetailedTelegramCalendar, LSTEP
from loader import bot
from states.UserState import UserStates
from telebot.types import Message
from utils.bot_logger import logger_bot
from translate import Translator


@bot.message_handler(state=UserStates.Adults)
def get_adult(message: Message):
    """
    Состояние записи количества персон и выбор даты въезда в отель
    :param message: Сообщение пользователя
    :return: None
    """
    logger_bot.log_info(f"Пользователь {message.from_user.id} отправил сообщение: {message.text}")
    if message.text.isdigit():
        bot.set_state(message.from_user.id, UserStates.Hotels, message.chat.id)
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['adults'] = message.text
            bot.send_message(message.from_user.id, f'Взрослых - {data["adults"]}\n'
                                                   f'Выберите день заезда в отель:')
        calendar, step = DetailedTelegramCalendar(min_date=datetime.date.today(),
                                                  calendar_id=0,
                                                  locale='ru').build()
        translator = Translator(from_lang="English", to_lang="russian")
        bot.send_message(message.chat.id,
                         f"Выберите {translator.translate(LSTEP[step])} заезда",
                         reply_markup=calendar)
    else:
        bot.send_message(message.chat.id, f'Введите число, например: 2')
