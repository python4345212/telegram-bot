from keyboards.inline.places import place
from loader import bot
from states.UserState import UserStates
from telebot.types import Message
from utils.bot_logger import logger_bot


@bot.message_handler(state=UserStates.City)
def take_city(message: Message):
    """
    Состояние записи города, его проверна и вывод на экран inline-клавитауры с уточнением города
    :param message: Сообщение пользователя
    :return: None
    """
    logger_bot.log_info(f"Пользователь {message.from_user.id} отправил сообщение: {message.text}")
    if not message.text.isalpha():
        bot.send_message(message.from_user.id, f'Цифр не может быть в названии, напишите еще раз')
    else:
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['city'] = message.text
        params = {"q": message.text, "locale": "ru_RU"}
        check_place = place(parameters=params, chat_id=message.chat.id)
        # Проверка существования страны через utils.check_smith.check_city_and_country
        if check_place is None:
            bot.send_message(message.from_user.id, f'Нет такого города!, Введите существующий и корректный город, '
                                                   f'например: Марсель')
        else:
            # Вызов состояния записи года отдыха
            bot.send_message(message.from_user.id, f'Уточните город: ', reply_markup=check_place)
            bot.set_state(message.from_user.id, UserStates.Adults, message.chat.id)
