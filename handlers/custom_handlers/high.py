from telebot.types import Message, ReplyKeyboardRemove
from translate import Translator

from database.adding import add_command, get_city_id, get_date_in, get_date_out, add_history, get_city_name
from keyboards.inline.hotels_list import hotels
from keyboards.reply.sort_parameters import sort_param, sort_method
from loader import bot
from states.UserState import UserStates
from utils.bot_logger import logger_bot
from utils.misc.api_get_hotels import hotels_list_request
from utils.misc.low_to_high_price_request import low_to_high_cost_request
from utils.misc.low_to_high_distance_request import request_for_distance_sort
from utils.misc.high_to_low_distance_request import request_distance_high_to_low
from utils.misc.high_to_low_price_request import price_high_to_low_request


@bot.message_handler(commands=["high"])
def low_high_command(message: Message):
    """
    Функция для команды "high". В последствии сортировка результатов от меньшего к большему или наоборот
    :param message: Сообщение пользователя
    :return: None
    """
    logger_bot.log_info(f"Пользователь {message.from_user.id} нажал команду /high")
    add_command(command=message.text, chat_id=message.chat.id)
    bot.set_state(message.from_user.id, UserStates.City, message.chat.id)
    bot.send_message(message.chat.id, f'Напишите город или населенный пункт, где хотите отдохнуть:')


@bot.message_handler(state=UserStates.SetFilter)
def counts_category(message: Message):
    """
    Состояние для запроса к API с целью сортировки от меньшего к большему или наоборот. Вывод результата пользователю
    :param message: Сообщение пользователя
    :return: None
    """
    logger_bot.log_info(f"Пользователь {message.from_user.id} отправил сообщение: {message.text}")
    if message.text == "От большего к меньшему":
        bot.set_state(message.from_user.id, UserStates.High, message.chat.id)
        bot.send_message(message.chat.id, f'Выберите параметр по которому провести сортировку:',
                         reply_markup=sort_param())
    elif message.text == "От меньшего к большему":
        bot.set_state(message.from_user.id, UserStates.Low, message.chat.id)
        bot.send_message(message.chat.id, f'Выберите параметр по которому провести сортировку:',
                         reply_markup=sort_param())
    else:
        bot.send_message(message.chat.id, f'Выберите одну из команд на клавиатуре',
                         reply_markup=sort_method())


@bot.message_handler(state=UserStates.Low)
def filter_low(message: Message):
    """
    Состояние для запроса к API с целью сортировки от меньшего к большему или наоборот. Вывод результата пользователю
    :param message: Сообщение пользователя
    :return: None
    """
    logger_bot.log_info(f"Пользователь {message.from_user.id} отправил сообщение: {message.text}")
    translator = Translator(from_lang="English", to_lang="russian")
    if message.text == 'Цена':
        bot.send_message(message.chat.id, f'Подождите пару секунд, загружаем фотографии...',
                         reply_markup=ReplyKeyboardRemove())
        with bot.retrieve_data(message.from_user.id) as data:
            hotel_list = low_to_high_cost_request(city_id=str(get_city_id(chat_id=message.chat.id)),
                                                  date_in=get_date_in(chat_id=message.chat.id),
                                                  date_out=get_date_out(chat_id=message.chat.id),
                                                  adults=data["adults"],
                                                  result_size=data["hotels"],
                                                  min_cost=1,
                                                  max_cost=1000000)
            for name_hotel in hotel_list.keys():
                photos = hotels_list_request(hotel_id=hotel_list[name_hotel]["hotels_id"],
                                             photos_count=int(data["photos"]))
                bot.send_media_group(message.chat.id, photos['photos'])
                bot.send_message(message.chat.id, f'Средняя оценка отеля: {hotel_list[name_hotel]["score"]}\n'
                                                  f'Итоговая цена: {round(hotel_list[name_hotel]["price"])} долларов\n'
                                                  f'Расстояние от центра: {hotel_list[name_hotel]["destination"]} миль\n'
                                                  f'Адрес отеля: {translator.translate(photos["location"])}',
                                 reply_markup=hotels(hotel_name=name_hotel,
                                                     hotel_id=hotel_list[name_hotel]["hotels_id"]))
            history_data = dict(
                city_id=get_city_name(chat_id=message.chat.id, city_id=str(get_city_id(message.chat.id))),
                date_in=get_date_in(chat_id=message.chat.id),
                date_out=get_date_out(chat_id=message.chat.id),
                adults=data["adults"],
                result_size=data["hotels"])
            add_history(chat_id=message.chat.id, info=history_data)
        bot.delete_state(message.from_user.id, message.chat.id)
        bot.send_message(message.chat.id, f'До скорых встреч!\n'
                                          f'Вы также можете перезапустить бота нажав на /start')
    elif message.text == 'Расстояние':
        bot.send_message(message.chat.id, f'Подождите пару секунд, загружаем фотографии...',
                         reply_markup=ReplyKeyboardRemove())
        with bot.retrieve_data(message.from_user.id) as data:
            hotel_list = request_for_distance_sort(city_id=str(get_city_id(chat_id=message.chat.id)),
                                                   date_in=get_date_in(chat_id=message.chat.id),
                                                   date_out=get_date_out(chat_id=message.chat.id),
                                                   adults=data["adults"],
                                                   result_size=data["hotels"],
                                                   min_cost=1,
                                                   max_cost=1000000)
            for name_hotel in hotel_list.keys():
                photos = hotels_list_request(hotel_id=hotel_list[name_hotel]["hotels_id"],
                                             photos_count=int(data["photos"]))
                bot.send_media_group(message.chat.id, photos['photos'])
                bot.send_message(message.chat.id, f'Средняя оценка отеля: {hotel_list[name_hotel]["score"]}\n'
                                                  f'Итоговая цена: {round(hotel_list[name_hotel]["price"])} долларов\n'
                                                  f'Расстояние от центра: {hotel_list[name_hotel]["destination"]} миль\n'
                                                  f'Адрес отеля: {translator.translate(photos["location"])}',
                                 reply_markup=hotels(hotel_name=name_hotel,
                                                     hotel_id=hotel_list[name_hotel]["hotels_id"]))
            history_data = dict(
                city_id=get_city_name(chat_id=message.chat.id, city_id=str(get_city_id(message.chat.id))),
                date_in=get_date_in(chat_id=message.chat.id),
                date_out=get_date_out(chat_id=message.chat.id),
                adults=data["adults"],
                result_size=data["hotels"])
            add_history(chat_id=message.chat.id, info=history_data)
        bot.delete_state(message.from_user.id, message.chat.id)
        bot.send_message(message.chat.id, f'До скорых встреч!\n'
                                          f'Вы также можете перезапустить бота нажав на /start')
    else:
        bot.send_message(message.chat.id, f'Выберите одну из команд на клавиатуре',
                         reply_markup=sort_param())


@bot.message_handler(state=UserStates.High)
def filter_high(message: Message):
    """
    Состояние для запроса к API с целью сортировки от большего к меньшему. Вывод результата пользователю
    :param message: Сообщение пользователя
    :return: None
    """
    translator = Translator(from_lang="English", to_lang="russian")
    logger_bot.log_info(f"Пользователь {message.from_user.id} отправил сообщение: {message.text}")
    if message.text == 'Цена':
        bot.send_message(message.chat.id, f'Подождите пару секунд, загружаем фотографии...',
                         reply_markup=ReplyKeyboardRemove())
        with bot.retrieve_data(message.from_user.id) as data:
            hotel_list = price_high_to_low_request(city_id=str(get_city_id(chat_id=message.chat.id)),
                                                   date_in=get_date_in(chat_id=message.chat.id),
                                                   date_out=get_date_out(chat_id=message.chat.id),
                                                   adults=data["adults"],
                                                   result_size=data["hotels"],
                                                   min_cost=1,
                                                   max_cost=1000000)
            for name_hotel in hotel_list.keys():
                photos = hotels_list_request(hotel_id=hotel_list[name_hotel]["hotels_id"],
                                             photos_count=int(data["photos"]))
                bot.send_media_group(message.chat.id, photos['photos'])
                bot.send_message(message.chat.id, f'Средняя оценка отеля: {hotel_list[name_hotel]["score"]}\n'
                                                  f'Итоговая цена: {round(hotel_list[name_hotel]["price"])} долларов\n'
                                                  f'Расстояние от центра: {hotel_list[name_hotel]["destination"]} миль\n'
                                                  f'Адрес отеля: {translator.translate(photos["location"])}',
                                 reply_markup=hotels(hotel_name=name_hotel,
                                                     hotel_id=hotel_list[name_hotel]["hotels_id"]))
            history_data = dict(
                city_id=get_city_name(chat_id=message.chat.id, city_id=str(get_city_id(message.chat.id))),
                date_in=get_date_in(chat_id=message.chat.id),
                date_out=get_date_out(chat_id=message.chat.id),
                adults=data["adults"],
                result_size=data["hotels"])
            add_history(chat_id=message.chat.id, info=history_data)
        bot.delete_state(message.from_user.id, message.chat.id)
        bot.send_message(message.chat.id, f'До скорых встреч!\n'
                                          f'Вы также можете перезапустить бота нажав на /start')
    elif message.text == 'Расстояние':
        bot.send_message(message.chat.id, f'Подождите пару секунд, загружаем фотографии...',
                         reply_markup=ReplyKeyboardRemove())
        with bot.retrieve_data(message.from_user.id) as data:
            hotel_list = request_distance_high_to_low(city_id=str(get_city_id(chat_id=message.chat.id)),
                                                      date_in=get_date_in(chat_id=message.chat.id),
                                                      date_out=get_date_out(chat_id=message.chat.id),
                                                      adults=data["adults"],
                                                      result_size=data["hotels"],
                                                      min_cost=1,
                                                      max_cost=1000000)
            for name_hotel in hotel_list.keys():
                photos = hotels_list_request(hotel_id=hotel_list[name_hotel]["hotels_id"],
                                             photos_count=int(data["photos"]))
                bot.send_media_group(message.chat.id, photos['photos'])
                bot.send_message(message.chat.id, f'Средняя оценка отеля: {hotel_list[name_hotel]["score"]}\n'
                                                  f'Итоговая цена: {round(hotel_list[name_hotel]["price"])} долларов\n'
                                                  f'Расстояние от центра: {hotel_list[name_hotel]["destination"]} миль\n'
                                                  f'Адрес отеля: {translator.translate(photos["location"])}',
                                 reply_markup=hotels(hotel_name=name_hotel,
                                                     hotel_id=hotel_list[name_hotel]["hotels_id"]))
            history_data = dict(
                city_id=get_city_name(chat_id=message.chat.id, city_id=str(get_city_id(message.chat.id))),
                date_in=get_date_in(chat_id=message.chat.id),
                date_out=get_date_out(chat_id=message.chat.id),
                adults=data["adults"],
                result_size=data["hotels"])
            add_history(chat_id=message.chat.id, info=history_data)
        bot.delete_state(message.from_user.id, message.chat.id)
        bot.send_message(message.chat.id, f'До скорых встреч!\n'
                                          f'Вы также можете перезапустить бота нажав на /start')
    else:
        bot.send_message(message.chat.id, f'Выберите одну из команд на клавиатуре',
                         reply_markup=sort_param())
