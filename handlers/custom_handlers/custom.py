from telebot.types import Message
from database.adding import get_city_id, get_date_in, get_date_out, add_command, add_history, get_city_name
from keyboards.inline.hotels_list import hotels
from loader import bot
from states.UserState import UserStates
from utils.bot_logger import logger_bot
from utils.misc.api_get_hotels import hotels_list_request
from utils.misc.best_deal_price import best_deal_cost_request
from translate import Translator


@bot.message_handler(commands=["custom"])
def custom_command(message: Message):
    """
    Функция для запроса города отдыха и выбора наилучшего места
    :param message: Сообщение пользователя
    :return: None
    """
    logger_bot.log_info(f"Пользователь {message.from_user.id} нажал команду /custom")
    add_command(command=message.text, chat_id=message.chat.id)
    bot.send_message(message.chat.id, f'Напишите город или населенный пункт, где хотите отдохнуть:')
    bot.set_state(message.from_user.id, UserStates.City, message.chat.id)


@bot.message_handler(state=UserStates.MinCost)
def take_min_cost(message: Message):
    """
    Состояние для записи минимальной стоимости номера и запроса максимальной стоимости номера
    :param message: Сообщение пользователя
    :return: None
    """
    logger_bot.log_info(f"Пользователь {message.from_user.id} отправил сообщение: {message.text}")
    if message.text.isdigit() >= 1:
        with bot.retrieve_data(message.from_user.id) as data:
            data['min_cost'] = message.text
            bot.set_state(message.from_user.id, UserStates.MaxCost, message.chat.id)
        bot.send_message(message.chat.id, f'Напишите максимальную стоимость номера, в долларах:')
    else:
        bot.send_message(message.chat.id, f'Введите число не менее единицы, например: 100')


@bot.message_handler(state=UserStates.MaxCost)
def take_max_cost(message: Message):
    """
    Состояние для записи максимальной стоимости номера и вывода отелей пользователю
    :param message: Сообщение пользователя
    :return: None
    """
    translator = Translator(from_lang="English", to_lang="russian")
    logger_bot.log_info(f"Пользователь {message.from_user.id} отправил сообщение: {message.text}")
    if message.text.isdigit():
        with bot.retrieve_data(message.from_user.id) as data:
            data['max_cost'] = message.text
            bot.send_message(message.chat.id, f'Подождите пару секунд, загружаем фотографии...')
            hotel_list = best_deal_cost_request(city_id=str(get_city_id(chat_id=message.chat.id)),
                                                date_in=get_date_in(chat_id=message.chat.id),
                                                date_out=get_date_out(chat_id=message.chat.id),
                                                adults=data["adults"],
                                                result_size=data["hotels"],
                                                min_cost=data["min_cost"],
                                                max_cost=data["max_cost"])
            for name_hotel in hotel_list.keys():
                photos = hotels_list_request(hotel_id=hotel_list[name_hotel]["hotels_id"],
                                             photos_count=int(data["photos"]))
                bot.send_media_group(message.chat.id, photos['photos'])
                bot.send_message(message.chat.id, f'Средняя оценка отеля: {hotel_list[name_hotel]["score"]}\n'
                                                  f'Итоговая цена: {round(hotel_list[name_hotel]["price"])} долларов\n'
                                                  f'Расстояние от центра: {hotel_list[name_hotel]["destination"]} миль\n'
                                                  f'Адрес отеля: {translator.translate(photos["location"])}',
                                 reply_markup=hotels(hotel_name=name_hotel,
                                                     hotel_id=hotel_list[name_hotel]["hotels_id"]))
            history_data = dict(
                city_id=get_city_name(chat_id=message.chat.id, city_id=str(get_city_id(message.chat.id))),
                date_in=get_date_in(chat_id=message.chat.id),
                date_out=get_date_out(chat_id=message.chat.id),
                adults=data["adults"],
                result_size=data["hotels"])
            add_history(chat_id=message.chat.id, info=history_data)
        bot.delete_state(message.from_user.id, message.chat.id)
        bot.send_message(message.chat.id, f'До скорых встреч!\n'
                                          f'Вы также можете перезапустить бота нажав на /start')
    else:
        bot.send_message(message.chat.id, f'Введите число, например: 500')
