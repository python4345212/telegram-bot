from telegram_bot_calendar import DetailedTelegramCalendar, LSTEP
import datetime
from database.adding import add_date_in
from handlers.custom_handlers.date_out import date_out
from loader import bot
from translate import Translator


@bot.callback_query_handler(func=DetailedTelegramCalendar.func(calendar_id=0))
def callback_date_in(callback):
    """
    Функция выбора даты въезда с помощью телеграм-календаря
    :param callback: Функция обработчик сообщения бота
    :return: None
    """
    result, key, step = DetailedTelegramCalendar(min_date=datetime.date.today(),
                                                 calendar_id=0,
                                                 locale='ru').process(callback.data)
    if not result and key:
        translator = Translator(from_lang="English", to_lang="russian")
        bot.edit_message_text(f"Выберите {translator.translate(LSTEP[step])} заезда",
                              callback.message.chat.id,
                              callback.message.message_id,
                              reply_markup=key)
    elif result:
        bot.edit_message_text(f'День заезда: {result}',
                              callback.message.chat.id,
                              callback.message.message_id
                              )
        add_date_in(date=str(result), chat_id=callback.message.chat.id)
        date_out(message=callback.message)
