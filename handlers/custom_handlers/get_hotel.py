from telebot.types import Message, ReplyKeyboardRemove
from loader import bot
from states.UserState import UserStates
from utils.bot_logger import logger_bot


@bot.message_handler(state=UserStates.Hotels)
def get_hotels(message: Message):
    """
    Состояние записи количества отелей, которые необходимо будет вывести.
    Сортировка отелей или лучший вариант отеля
    :param message: Сообщение пользователя
    :return: None
    """
    logger_bot.log_info(f"Пользователь {message.from_user.id} отправил сообщение: {message.text}")
    if message.text.isdigit() and 1 <= int(message.text) <= 3:
        bot.set_state(message.from_user.id, UserStates.PhotosCount, message.chat.id)
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['hotels'] = message.text
        bot.send_message(message.chat.id, f"Сколько фотографий отеля хотите увидеть? Не менее 2 и не более 10",
                         reply_markup=ReplyKeyboardRemove())
    else:
        bot.send_message(message.chat.id, f'Введите число не более 3, например: 2')
