import datetime
from telegram_bot_calendar import DetailedTelegramCalendar, LSTEP
from telebot.types import Message
from database.adding import add_date_out, get_date_in
from loader import bot
from states.UserState import UserStates
from translate import Translator


def date_out(message: Message):
    """
    Функция выбора даты выезда с помощью телеграм-календаря
    :param message: Функция обработчик сообщения бота
    :return: None
    """
    calendar_1, step_1 = DetailedTelegramCalendar(
        min_date=datetime.datetime.strptime(get_date_in(chat_id=message.chat.id), '%Y-%m-%d').date(),
        calendar_id=1,
        locale='ru').build()
    translator = Translator(from_lang="English", to_lang="russian")
    bot.send_message(message.chat.id,
                     f"Выберите {translator.translate(LSTEP[step_1])} отъезда",
                     reply_markup=calendar_1)


@bot.callback_query_handler(func=DetailedTelegramCalendar.func(calendar_id=1))
def callback_day_out(callback):
    result, key, step = DetailedTelegramCalendar(
        min_date=datetime.datetime.strptime(get_date_in(chat_id=callback.message.chat.id), '%Y-%m-%d').date(),
        calendar_id=1,
        locale='ru').process(callback.data)
    if not result and key:
        translator = Translator(from_lang="English", to_lang="russian")
        bot.edit_message_text(f"Выберите {translator.translate(LSTEP[step])} отъезда",
                              callback.message.chat.id,
                              callback.message.message_id,
                              reply_markup=key)
    elif result:
        bot.set_state(callback.from_user.id, UserStates.Hotels, callback.message.chat.id)
        bot.edit_message_text(f'День отъезда: {result}',
                              callback.message.chat.id,
                              callback.message.message_id
                              )
        bot.send_message(callback.message.chat.id, f"Сколько отелей вывести в результате? Не более трех.")
        add_date_out(date=str(result), chat_id=int(callback.message.chat.id))
