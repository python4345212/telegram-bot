from database.adding import get_city_name, add_city_id
from loader import bot
from utils.bot_logger import logger_bot


@bot.callback_query_handler(func=lambda call: call.data.isdigit())
def callback_city(call):
    """
    Функция обработки данных при нажатии на inline-кнопку пользователем и запись данных в базу
    :param call: Функция обработки сообщения бота
    :return: None
    """
    logger_bot.log_info(f"Пользователь {call.message.from_user.id} изменил сообщение: {call.message.text}")
    bot.edit_message_text(text=f'Населенный пункт - {get_city_name(chat_id=call.message.chat.id, city_id=call.data)}\n'
                          f'Сколько взрослых?',
                          chat_id=call.message.chat.id,
                          message_id=call.message.message_id,)
    add_city_id(city_id=call.data, chat_id=call.message.chat.id)
