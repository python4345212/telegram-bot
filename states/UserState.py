from telebot.handler_backends import State, StatesGroup


class UserStates(StatesGroup):
    """
    Класс состояний
    """
    Custom = State()
    City = State()
    Hotels = State()
    DateIn = State()
    DateOut = State()
    Adults = State()
    CountsCategory = State()
    MinCost = State()
    MaxCost = State()
    PhotosCount = State()
    SetFilter = State()
    High = State()
    Low = State()
