from utils.bot_logger import logger_bot
from utils.misc.requests_to_api import api_request


def find(list_with_result: list) -> list:
    """
    Проверка города на существование и является ли выбранное место городом, а не страной и прочим
    :param list_with_result: Список мест, которые удалось найти, в том числе: города, отели, страны
    :return: Отсортированный список
    """
    names_cities = list()
    for city_place in list_with_result:
        if city_place['type'] == 'CITY':
            name_of_city = city_place.get('regionNames', {}).get('fullName')
            city_id = city_place.get('gaiaId')
            names_cities.append({'city_name': name_of_city, 'city_id': city_id})
    logger_bot.log_info(f"Проверка места прошла успешно, результат - {names_cities}")
    return names_cities


def check_populated_area(parameters: dict) -> list | None:
    """
    Проверка города на существование и составление списка найденных городов
    :param parameters: Словарь с названием города и языком локализации
    :return: При отсутствии результата None, при успешном поиске лист с названиями городов и уточнением адреса
    """
    response = api_request(method_endswith='locations/v3/search', params=parameters, method_type='GET')
    city = response.get('sr')
    if len(city) > 1:
        cities = find(list_with_result=city)
        logger_bot.log_info(f"Проверка места прошла успешно, результат - {cities}")
        return cities
    else:
        logger_bot.log_debug(f"Место введенное пользователем не обнаружено, вернули None")
        return None
