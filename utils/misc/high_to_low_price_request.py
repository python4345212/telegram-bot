from datetime import datetime
from utils.bot_logger import logger_bot
from utils.misc.requests_to_api import api_request


def price_high_to_low_request(city_id: str,
                              date_in: str,
                              date_out: str,
                              adults: int,
                              result_size: int,
                              min_cost: int,
                              max_cost: int) -> dict:
    """
    Функция запроса к API и получения названий отелей
    :param city_id: Идентификатор города
    :param date_in: Дата въезда в отель выбранная пользователем
    :param date_out: Дата выезда из отеля выбранная пользователем
    :param adults: Количество персон
    :param result_size: Количество отелей, которые необходимо вывести
    :param min_cost: Минимальная цена номера
    :param max_cost: Максимальная цена номера
    :return: Словарь с ценой, фотографией, нзванием и оценкой отеля
    """
    start_day = datetime.strptime(date_in, '%Y-%m-%d')
    end_day = datetime.strptime(date_out, '%Y-%m-%d')
    payload = {
        "currency": "USD",
        "eapid": 1,
        "locale": "en_US",
        "siteId": 300000001,
        "destination": {"regionId": city_id},
        "checkInDate": {
            "day": start_day.day,
            "month": start_day.month,
            "year": start_day.year
        },
        "checkOutDate": {
            "day": end_day.day,
            "month": end_day.month,
            "year": end_day.year
        },
        "rooms": [
            {
                "adults": int(adults),
                "children": []
            }
        ],
        "resultsStartingIndex": 0,
        "resultsSize": 200,
        "sort": "PRICE_LOW_TO_HIGH",
        "filters": {"price": {
            "max": int(max_cost),
            "min": int(min_cost)
        }}
    }
    response = api_request(method_endswith="properties/v2/list", params=payload, method_type='POST')
    hotels_list = response.get('data', {}).get('propertySearch', {}).get('properties')
    hotels_list.reverse()
    hotels = dict()
    for number_hotel, hotel in enumerate(hotels_list):
        if number_hotel == int(result_size):
            return hotels
        hotels_name = hotel.get('name', 'No name')
        logger_bot.log_info(f"Имя отеля - {hotels_name}")
        hotels[hotels_name] = dict()
        hotels[hotels_name]['destination'] = (hotel.get('destinationInfo', 'No id')
                                              .get('distanceFromDestination', 'No id').get('value'))
        logger_bot.log_info(f"Расстояние отеля от центра - {hotels[hotels_name]['destination']}")
        hotels[hotels_name]['hotels_id'] = hotel.get('id', 'No id')
        logger_bot.log_info(f"Идентификатор отеля - {hotels[hotels_name]['hotels_id']}")
        hotels[hotels_name]['price'] = hotel.get('price', {}).get('lead', {}).get('amount')
        logger_bot.log_info(f"Цена отеля - {hotels[hotels_name]['price']}")
        hotels[hotels_name]['score'] = hotel.get('reviews', {}).get('score', 'No score')
        logger_bot.log_info(f"Оценка отеля - {hotels[hotels_name]['score']}")
    return hotels
