import requests
from loguru import logger
from typing import Optional
from config_data.config import head
from utils.bot_logger import logger_bot


def api_request(method_endswith: str,
                params: dict,
                method_type: str,
                ):
    """
    Функция-распределение запроса к API
    :param method_endswith: Окончание url сайта зависит от варианта запроса
    :param params: Параметры для запроса к API
    :param method_type: Метод запроса 'GET' или 'POST'
    :return: результат выполнения запроса в формате .json()
    """
    url = f"https://hotels4.p.rapidapi.com/{method_endswith}"

    # В зависимости от типа запроса вызываем соответствующую функцию
    if method_type == 'GET':
        return get_request(
            url=url,
            param=params,
            header=head
        )
    elif method_type == 'POST':
        return post_request(
            url=url,
            param=params
        )


def get_request(url: str, param: any, header: dict) -> Optional[dict]:
    """
    GET-запрос к API
    :param url: URL-адрес для запроса информации
    :param param: Параметры для запроса к API
    :param header: Словарь с ключом API и адресом сайта
    :return: результат выполнения запроса в формате .json()
    """
    try:
        response = requests.get(
            url=url,
            headers=header,
            params=param,
            timeout=10
        )
    except requests.RequestException as ReqError:
        logger_bot.log_error(f"Возникла ошибка - {ReqError}")
        return None
    if response.status_code == requests.codes.ok:
        logger_bot.log_info(f"GET-запрос выполнен успешно")
        return response.json()


def post_request(url: str, param: any) -> Optional[dict]:
    """
    POST-запрос к API
    :param url: URL-адрес для запроса информации
    :param param: Параметры для запроса к API
    :return: результат выполнения запроса в формате .json()
    """
    headers = head.copy()
    headers["content-type"] = "application/json"
    try:
        response = requests.post(url, json=param, headers=headers)
    except requests.RequestException as ReqError:
        logger_bot.log_error(f"Возникла ошибка - {ReqError}")
        return None
    if response.status_code == requests.codes.ok:
        logger_bot.log_info(f"POST-запрос выполнен успешно")
        return response.json()
