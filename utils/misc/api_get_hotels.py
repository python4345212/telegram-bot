from telebot.types import InputMediaPhoto
from utils.bot_logger import logger_bot
from utils.misc.requests_to_api import api_request


def hotels_list_request(hotel_id: str, photos_count: int) -> dict:
    """
    Функция-запрос к API с получением детальной информации об отеле
    :param photos_count: Количество фотографий
    :param hotel_id: Идентификатор отеля
    :return: Словарь с названием отеля и адресом
    """
    payload = {
            "currency": "USD",
            "eapid": 1,
            "locale": "en_US",
            "siteId": 300000001,
            "propertyId": hotel_id
        }
    response = api_request(method_endswith='properties/v2/detail', params=payload, method_type='POST')
    images = response.get('data', {}).get('propertyInfo', {}).get('propertyGallery', {}).get('images')
    hotel_photos = list()
    for ph_id, photo in enumerate(images):
        if photos_count == ph_id:
            logger_bot.log_info(f"Фотографии собраны, результат - {hotel_photos}")
            break
        hotel_photos.append(InputMediaPhoto(photo.get('image', {}).get('url')))
    hotel_detail = dict()
    hotel_detail['photos'] = hotel_photos
    hotel_detail['location'] = (response.get('data', {}).get('propertyInfo', {}).get('summary', {}).get('location', {})
                                .get('address').get('addressLine'))
    logger_bot.log_info(f"Выбранное место - {hotel_detail['location']}")
    return hotel_detail
