from peewee import Model, TextField, PrimaryKeyField, IntegerField, ForeignKeyField
from playhouse.sqlite_ext import SqliteDatabase, JSONField

db = SqliteDatabase('database/base/bot_DB.db')


class BaseModel(Model):
    class Meta:
        database = db


class UserInfo(BaseModel):
    """
    Класс для создания таблицы и полей в базе даннных. Информация о пользователе
    """
    id = PrimaryKeyField(primary_key=True)
    name = TextField()
    chat_id = IntegerField()
    command = TextField(default="")
    city_name = JSONField(default=0)
    city_id = IntegerField(default=0)
    hotel_name = TextField(default="")

    class Meta:
        db_table = 'Users'


class Date(BaseModel):
    """
    Класс для создания таблицы и полей в базе даннных. День заезда в отель и день выезда
    """
    id = PrimaryKeyField(primary_key=True)
    chat_id = ForeignKeyField(UserInfo, field=UserInfo.chat_id)
    date_in = TextField(default='None')
    date_out = TextField(default='None')

    class Meta:
        db_table = 'Date'


class History(BaseModel):
    """
    Класс для создания таблицы и полей в базе даннных. История запросов пользователя
    """
    id = PrimaryKeyField(primary_key=True)
    chat_id = ForeignKeyField(UserInfo, field=UserInfo.chat_id)
    info_string = JSONField(default=0)

    class Meta:
        db_table = 'History'
