from database.database import UserInfo, Date, db, History
from utils.bot_logger import logger_bot

db.connect()
db.create_tables([UserInfo, Date, History])


def add_date_in(date: str, chat_id: int) -> None:
    """
    Запись и получение данных о дне заезда в отель.
    :param date: дата заезда выбранная пользователем в формате -> "24-09-2009"
    :param chat_id: ID чата с пользователем
    :return: Ничего не возвращает
    """
    if Date.get_or_none(chat_id == Date.chat_id) is not None:
        Date.update(date_in=date).where(Date.chat_id == chat_id).execute()
        logger_bot.log_info('Обновили дату въезда пользователя - {date} в базе данных'.format(date=date))
    else:
        Date(date_in=date, chat_id=chat_id).save()
        logger_bot.log_info('Добавили дату въезда пользователя - {date} в базу данных'.format(date=date))
    db.close()


def get_date_in(chat_id: int) -> str:
    """
    Получение данных о дне заезда в отель.
    :param chat_id: ID чата с пользователем
    :return: дата заезда выбранная пользователем в формате -> "24-09-2009"
    """
    date_in = Date.get(Date.chat_id == chat_id).date_in
    db.close()
    logger_bot.log_info('Вывели дату въезда - {date_in} из базы данных'.format(date_in=date_in))
    return date_in


def add_date_out(date: str, chat_id: int) -> None:
    """
    Запись данных о дне выезда из отеля.
    :param date: дата выезда выбранная пользователем в формате -> "24-09-2009"
    :param chat_id: ID чата с пользователем
    :return: Ничего не возвращает
    """
    if Date.get_or_none(chat_id == Date.chat_id) is not None:
        Date.update(date_out=date).where(Date.chat_id == chat_id).execute()
        logger_bot.log_info('Обновили дату выезда пользователя - {date} в базе данных'.format(date=date))
    else:
        Date(date_out=date, chat_id=chat_id).save()

        logger_bot.log_info('Добавили дату выезда пользователя - {date} в базу данных'.format(date=date))
    db.close()


def get_date_out(chat_id: int) -> str:
    """
    Получение данных о дне выезда из отеля.
    :param chat_id: ID чата с пользователем
    :return: дата выезда выбранная пользователем в формате -> "24-09-2009"
    """
    date_out = Date.get(Date.chat_id == chat_id).date_out
    db.close()
    logger_bot.log_info('Вывели дату выезда - {date_out} из базы данных'.format(date_out=date_out))
    return date_out


def add_city_name(city_name: list, chat_id: int) -> None:
    """
    Запись данных о названии города.
    :param city_name: Название городов в виде словаря ключ - id, значение - название города.
    :param chat_id: ID чата с пользователем.
    :return: Ничего не возвращает.
    """
    UserInfo.update(city_name=city_name).where(UserInfo.chat_id == chat_id).execute()
    db.close()
    logger_bot.log_info('Добавили название города - {city_name} в базу данных'
                        .format(city_name=city_name))


def get_city_name(city_id: str, chat_id: int) -> str:
    """
    Получение данных о названии города.
    :param city_id: ID города.
    :param chat_id: ID чата с пользователем.
    :return: Название города.
    """
    for city in UserInfo.get(UserInfo.chat_id == chat_id).city_name:
        if city["city_id"] == city_id:
            logger_bot.log_info('Вернули название города - {city}'.format(city=city["city_id"]))
            return city["city_name"]


def add_city_id(city_id: int, chat_id: int) -> None:
    """
    Запись данных об идентификаторе города.
    :param city_id: ID города.
    :param chat_id: ID чата с пользователем.
    :return: Ничего не возвращает.
    """
    UserInfo.update(city_id=city_id).where(UserInfo.chat_id == chat_id).execute()
    db.close()
    logger_bot.log_info('Добавили идентификатор города - {city_id} в базу данных'
                        .format(city_id=city_id))


def get_city_id(chat_id: int) -> int:
    """
    Получение данных об идентификаторе города.
    :param chat_id: ID чата с пользователем.
    :return: Возвращает ID города выбранного пользователем.
    """
    city_id = UserInfo.get(UserInfo.chat_id == chat_id).city_id
    db.close()
    logger_bot.log_info('Вывели идентификатор города - {city_id} из базы данных'
                        .format(city_id=city_id))
    return city_id


def add_command(command: str, chat_id: int) -> None:
    """
    Запись данных о введенной команде пользователя.
    :param command: Название команды введеной пользователем /high | /custom.
    :param chat_id: ID чата с пользователем.
    :return: Ничего не возвращает.
    """
    UserInfo.update(command=command).where(UserInfo.chat_id == chat_id).execute()
    db.close()
    logger_bot.log_info('Добавили введенную команду пользователем - {command} в базу данных'
                        .format(command=command))


def get_command(chat_id: int) -> str:
    """
    Получение данных о введенной команде пользователя.
    :param chat_id: ID чата с пользователем.
    :return: азвание команды введеной пользователем /high | /custom.
    """
    command = UserInfo.get(UserInfo.chat_id == chat_id).command
    db.close()
    logger_bot.log_info('Вывели введенную команду пользователя - {command} из базы данных'
                        .format(command=command))
    return command


def add_user(user_name: str, chat_id: int) -> None:
    """
    Запись данных о пользователе.
    :param user_name: Имя пользователя.
    :param chat_id: ID чата с пользователем.
    :return: Ничего не возвращает.
    """
    if UserInfo.get_or_none(chat_id == UserInfo.chat_id) is not None:
        UserInfo.update(name=user_name).where(UserInfo.chat_id == chat_id).execute()
        logger_bot.log_info('Обновили данные пользователя - {user_name}'.format(user_name=user_name))
    else:
        UserInfo.create(name=user_name, chat_id=chat_id)
        logger_bot.log_info('Добавили нового пользователя {user_name} в базу данных'
                            .format(user_name=user_name))
    db.close()


def add_history(info: dict, chat_id: int) -> None:
    """
    Запись истории запросов пользователя в базу данных.
    :param info: История запросов пользователя.
    :param chat_id: ID чата с пользователем.
    :return: Ничего не возвращает.
    """
    if History.select().count() == 10:
        History.get(History.id == 1).delete_instance()
        History(info_string=info, chat_id=chat_id).save()
        logger_bot.log_info('Удалили самый старый запрос пользователя. '
                            'Добавили новый запрос пользователя в историю'
                            '{info}'.format(info=info))
    else:
        History(info_string=info, chat_id=chat_id).save()
        logger_bot.log_info('Добавили запрос пользователя в историю - {info}'.format(info=info))
    db.close()


def get_history(chat_id: int) -> tuple | None:
    """
    Получение истории запросов пользователя из базы данных
    :param chat_id: ID чата с пользователем
    :return: История запросов пользователя
    """
    if History.get_or_none(History.chat_id == chat_id) is None:
        db.close()
        logger_bot.log_debug('Пользователь пытался вывести пустую историю запросов')
        return None
    else:
        user_history = History.select().where(History.chat_id == chat_id)
        history_to_return = tuple()
        for story in user_history:
            history_to_return += ({
                'info': story.info_string
            }, )
        db.close()
        logger_bot.log_info('Пользователь успешно вывел историю запросов. '
                            'Количество запросов: {length_history}'
                            .format(length_history=len(history_to_return)))
        return history_to_return
